//Mock Data
let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]

const http = require('http');


http.createServer((req, res) => {


    console.log(req.method);


    if (req.url === '/items' && req.method === "GET") {


        res.writeHead(200, { 'Content-Type': 'application/json' });


        res.end(JSON.stringify(items));

    }


    if (req.url === '/items' && req.method === "POST") {



        let requestBody = "";



        req.on('data', function(data) {

            requestBody += data;

        })


        req.on('end', function() {


            requestBody = JSON.parse(requestBody);


            items.push(requestBody);



            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(items));

        })

    }

    //Stretch Goal

    if (req.url === '/items' && req.method === "DELETE") {

        let requestBody = "";

        req.on('data', function(data) {

            requestBody += data;

        })

        req.on('end', function() {


            requestBody = JSON.parse(requestBody);


            items.pop(requestBody);

            res.writeHead(200, { 'Content-Type': 'application/json' })
            res.end(JSON.stringify(items));

        })

    }

}).listen(8000);




console.log(`Server is running on localhost:8000`);
