// Node.js Routing with HTTP Methods

// CRUD Operations			HTTP Methods
	// C- reate				POST
	// R- ead				GET
	// U- pdate				PUT / PATCH
	// D- elete				DELETE

/*
	Not only can we get the request url endpoint to differentiate requests but we can also check for the request method.

	We can think of request methods as the request/client telling our server the action to take for their request.

	This way we don't need to have different endpoints for everything that our client wants to do.

	HTTP methods allow us to group and organize our routes.
	
	With this we can have the same endpoints but with different methods.

	HTTP methods are particularly and primarily concerned with CRUD operations.

	Common HTTP Methods:
	
	GET - Get method for request indicates that the client/request wants to retrieve or get data.

	POST - Post method for request indicates that the client/request wants to post data and create a document.

	PUT - Put method for request indicates that the client/request wants to input data and update a document.

	DELETE - Delete method for request indicates that the client/request wants to delete a document.

*/


const http = require('http');

// Mock data for users and courses

let users = [
	{
		username: "peterIsHomeless",
		email: "peterParker@mail.com",
		password: "peterNoWayHome"
	},
	{
		username: "Tony3000",
		email: "starksIndustries@gmail.com",
		password: "ironManWillBeBack"
	}
];

let courses = [
	{
		name: "Math 103",
		price: 2500,
		isActive: true
	},
	{
		name: "Biology 201",
		price: 2500,
		isActive: true
	}
];



http.createServer((req, res) => {

	if(req.url === "/" && req.method === "GET"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking GET method');

	} else if(req.url === "/" && req.method === "POST"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is checking a POST method');

	} else if(req.url === "/" && req.method === "PUT"){
		 
		 res.writeHead(200, {'Content-Type' : 'text/ plain'});
		 res.end('This route is for checking a PUT method');

	} else if(req.url === "/" && req.method === "DELETE"){

		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking a DELETE method');

	} else if(req.url === "/users" && req.method === "GET"){

		// Change the value of Content-Type header if we are passing json as our server response: 'application/json'
		res.writeHead(200, {'Content-Type' : 'application/json'});

		// We cannot pass other data type as a response except for strings
		// To be able to pass the array of users, first we stringify the array as JSON
		res.end(JSON.stringify(users));

	} else if(req.url === "/courses" && req.method === "GET"){

		res.writeHead(200, {'Content-Type' : 'application/json'});

		res.end(JSON.stringify(courses));

	} else if(req.url === "/users" && req.method === "POST"){

		let requestBody = "";

		// Receiving data from client to nodejs server requires 2 steps:

		// data step- this part will read the stream of data from our client and process the incoming data into the requestBody variable

		req.on('data', (data) => {

			console.log(data)

			requestBody += data
		})

		// end step- will run once or after the request has been completely sent from our client

		req.on('end', () => {

			console.log(requestBody);

			requestBody = JSON.parse(requestBody)

			let newUser = {

				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(users))
		})

	} else if(req.url === "/courses" && req.method === "POST"){

		let requestBody = "";

		// data-step
		req.on('data', (data) => {

			requestBody += data
		})

		// end-step

		req.on('end', () => {

			requestBody = JSON.parse(requestBody);

			let newCourse = {

				name: requestBody.name,
				price: requestBody.price,
				isActive: requestBody.isActive
			}

			courses.push(newCourse);

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(courses))
		})

	}


}).listen(4000);

console.log('Server is running on localhost:4000')

/*
	Mini Activity

	>> Create a condition that when "/" endpoint is accessed it will have a response of 
		>> "This is for checking DELETE method"

	>> Make the request method as delete and writehead should have status code 200 and a content type of text/plain

	>> Check the endpoint and method in Postman Client

	>> Send your output in our Hangouts

*/

/*
	Mini Activity

	>> Create a new route with GET method

	>> This route is on "/courses" endpoint and it is a GET method request
		>> status code: 200
		>> headers: Content-Type: application/json
		>> end the response with end() and send the courses array as JSON into our client

	>> Send your response screenshot in Hangouts
*/

/*
	Mini-Activity

	>> Create a new route with POST method

	>> This route is on "/courses" endpoint and it is a POST method request.
		>> status code: 200
		>> headers: Content-Type: 'application/ json'
		>> receive the request body from our postman client with our 2 step functions: data step and end step
		>> simulate the creation of a new course document and add the new object into the courses array
		>> end the response with end() and send the updated courses array as JSON into our client
		>> use the POST method route for course creation

	>> Take a screenshot of the response and send it to our hangouts
	

*/
